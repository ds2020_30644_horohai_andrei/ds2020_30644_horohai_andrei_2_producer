package com.example.rabbitmqsender.model;


import com.fasterxml.jackson.annotation.JsonProperty;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

public class SensorMessage {
    public  long pacientId;
    public  String startDate;
    public  String endDate;
    public  String activity;

    public SensorMessage(){}


    public SensorMessage(@JsonProperty("pacientId") final long pacientId,
                         @JsonProperty("startDate") final String startDate,
                         @JsonProperty("endDate") final String endDate,
                         @JsonProperty("activity") final String activity)
    {
        this.pacientId = pacientId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.activity = activity;
    }

    public long getPacientId() {
        return pacientId;
    }

    public void setPacientId(long pacientId){
        this.pacientId=pacientId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate){
        this.startDate=startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate){
        this.endDate=endDate;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity){
        this.activity=activity;
    }

    @Override
    public String toString() {
        return " SensorMessage {" +
                "pacientId=" + pacientId +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", activity='" + activity + '\'' +
                '}';
    }
}
