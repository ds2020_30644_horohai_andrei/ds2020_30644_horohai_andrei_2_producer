package com.example.rabbitmqsender;

import com.example.rabbitmqsender.model.SensorMessage;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Scanner;

@Component
public class Runner implements CommandLineRunner {

    @Value("${sensor.rabbitmq.exchange}")
    private String exchange;

    @Value("${sensor.rabbitmq.routingkey}")
    private String routingkey;

    private final RabbitTemplate rabbitTemplate;

    public Runner(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Sending message...");
        File fileName = new File("src/main/resources/static/activity.txt");

        Scanner scanner = new Scanner(fileName);
        while(scanner.hasNextLine())
        {
            String data[] = scanner.nextLine().split("\t\t");
            SensorMessage sensorMessage = new SensorMessage(1, data[0], data[1], data[2].trim());
            rabbitTemplate.convertAndSend(exchange, routingkey, sensorMessage);
            System.out.println("Sensor message sent" + sensorMessage.toString());


            Thread.sleep(1000);
        }
    }
}
